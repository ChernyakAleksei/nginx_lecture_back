let config  = require('./config.json')
let env = process.env.NODE_ENV || 'development';

module.exports = config[env];
